<?php
use PHPUnit\Framework\TestCase;

class HelloTest extends TestCase
{
    public function test_sayHello()
    {
        $this->assertSame('Hello David!', sayHello('David'));
    }
}
